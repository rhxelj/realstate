webpackJsonp([0],{

/***/ 100:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(16);

var _react2 = _interopRequireDefault(_react);

var _listingsData = __webpack_require__(54);

var _listingsData2 = _interopRequireDefault(_listingsData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Listings = function (_Component) {
  _inherits(Listings, _Component);

  function Listings() {
    _classCallCheck(this, Listings);

    var _this = _possibleConstructorReturn(this, (Listings.__proto__ || Object.getPrototypeOf(Listings)).call(this));

    _this.state = {
      name: 'RMC'
    };
    _this.loopListings = _this.loopListings.bind(_this);
    return _this;
  }

  _createClass(Listings, [{
    key: 'loopListings',
    value: function loopListings() {
      var listingsData = this.props.listingsData;


      if (listingsData == undefined || listingsData.length == 0) {
        return "Sorry your filter did not match any listing";
      }

      return listingsData.map(function (listing, index) {
        return _react2.default.createElement(
          'div',
          { className: 'card col-md-3', key: index },
          _react2.default.createElement(
            'div',
            { className: 'listing' },
            _react2.default.createElement(
              'div',
              { className: 'listing-img', style: { background: 'url(\'' + listing.image + '\') no-repeat center center' } },
              _react2.default.createElement(
                'span',
                { className: 'adress' },
                listing.address
              ),
              _react2.default.createElement(
                'div',
                { className: 'details' },
                _react2.default.createElement(
                  'div',
                  { className: 'col-md-3' },
                  _react2.default.createElement('div', { className: 'user-img' })
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'col-md-9' },
                  _react2.default.createElement(
                    'div',
                    { className: 'user-details' },
                    _react2.default.createElement(
                      'span',
                      { className: 'user-name' },
                      'Nina Smith'
                    ),
                    _react2.default.createElement(
                      'span',
                      { className: 'post-date' },
                      '05/05/2017'
                    )
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: 'listing-details' },
                    _react2.default.createElement(
                      'div',
                      { className: 'floor-space' },
                      _react2.default.createElement('i', { className: 'far fa-square' }),
                      ' ',
                      _react2.default.createElement(
                        'span',
                        null,
                        listing.floorSpace,
                        ' m\xB2'
                      )
                    ),
                    _react2.default.createElement(
                      'div',
                      { className: 'bedrooms' },
                      _react2.default.createElement('i', { className: 'fas fa-bed' }),
                      _react2.default.createElement(
                        'span',
                        null,
                        listing.rooms,
                        ' Bedrooms'
                      ),
                      ' '
                    ),
                    _react2.default.createElement('div', { className: '' }),
                    _react2.default.createElement('div', { className: '' })
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: 'view-btn' },
                    'View Listing'
                  )
                )
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'bottom-info' },
              _react2.default.createElement(
                'span',
                { className: 'price' },
                '$ ',
                listing.price,
                ' / month'
              ),
              _react2.default.createElement(
                'span',
                { className: 'location' },
                _react2.default.createElement('i', { className: 'fas fa-map-marker' }),
                listing.city
              )
            )
          )
        );
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'section',
        { id: 'listings' },
        _react2.default.createElement(
          'section',
          { className: 'search-area' },
          _react2.default.createElement('input', { type: 'text', name: 'search' })
        ),
        _react2.default.createElement(
          'section',
          { className: 'sortby-area' },
          _react2.default.createElement(
            'div',
            { className: 'results' },
            ' ',
            this.props.globalState.searchResult,
            ' results found'
          ),
          _react2.default.createElement(
            'div',
            { className: 'sort-options' },
            _react2.default.createElement(
              'select',
              { name: 'sortby', className: 'sortby', onChange: this.props.change },
              _react2.default.createElement(
                'option',
                { value: 'price-asc' },
                'Lowest Price'
              ),
              _react2.default.createElement(
                'option',
                { value: 'price-dsc' },
                'Highest Price'
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'view' },
              _react2.default.createElement('i', { className: 'fas fa-list' }),
              _react2.default.createElement('i', { className: 'fas fa-th' })
            )
          )
        ),
        _react2.default.createElement(
          'section',
          { className: 'listing-results' },
          this.loopListings()
        ),
        _react2.default.createElement(
          'section',
          { id: 'pagination' },
          _react2.default.createElement(
            'ul',
            { className: 'pages' },
            _react2.default.createElement(
              'li',
              null,
              'Prev'
            ),
            _react2.default.createElement(
              'li',
              { className: 'active' },
              '1'
            ),
            _react2.default.createElement(
              'li',
              null,
              '2'
            ),
            _react2.default.createElement(
              'li',
              null,
              '3'
            ),
            _react2.default.createElement(
              'li',
              null,
              '4'
            ),
            _react2.default.createElement(
              'li',
              null,
              '5'
            ),
            _react2.default.createElement(
              'li',
              null,
              '6'
            ),
            _react2.default.createElement(
              'li',
              null,
              'Next'
            )
          )
        )
      );
    }
  }]);

  return Listings;
}(_react.Component);

exports.default = Listings;

/***/ }),

/***/ 102:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(16);

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(33);

var _reactDom2 = _interopRequireDefault(_reactDom);

var _Header = __webpack_require__(99);

var _Header2 = _interopRequireDefault(_Header);

var _Filter = __webpack_require__(98);

var _Filter2 = _interopRequireDefault(_Filter);

var _Listings = __webpack_require__(100);

var _Listings2 = _interopRequireDefault(_Listings);

var _listingsData = __webpack_require__(54);

var _listingsData2 = _interopRequireDefault(_listingsData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var App = function (_Component) {
  _inherits(App, _Component);

  function App() {
    _classCallCheck(this, App);

    var _this = _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).call(this));

    _this.state = {
      name: 'RMC',
      listingsData: _listingsData2.default,
      neighbourhood: "All",
      houseType: "All",
      interior: "All",
      floor_price: 0,
      floor_space: 0,
      floor_radius: 0,
      filterData: _listingsData2.default,
      searchResult: _listingsData2.default.length,
      populateFormsData: {},
      sortby: "price-asc"
    };

    _this.change = _this.change.bind(_this);
    _this.filterData = _this.filterData.bind(_this);
    _this.populateForms = _this.populateForms.bind(_this);

    return _this;
  }

  _createClass(App, [{
    key: 'change',
    value: function change(event) {
      var _this2 = this;

      var name = event.target.name;
      var value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;

      this.setState(_defineProperty({}, name, value), function () {
        console.log(_this2.state);
        _this2.filterData();
      });
    }
  }, {
    key: 'filterData',
    value: function filterData() {
      var _this3 = this;

      var newData = this.state.listingsData.filter(function (item) {
        return item.price >= _this3.state.floor_price && item.floorSpace >= _this3.state.floor_space;
      });

      if (this.state.neighbourhood != "All") {
        newData = newData.filter(function (item) {
          return item.neighbourhood == _this3.state.neighbourhood;
        });
      }

      if (this.state.houseType != "All") {
        newData = newData.filter(function (item) {
          return item.houseType == _this3.state.houseType;
        });
      }

      if (this.state.sortby == "price-asc") {
        var listingsData = this.state.listingsData.sort(function (a, b) {
          return b.price - a.price;
        });

        this.setState({
          listingsData: listingsData
        });
      }

      if (this.state.sortby == "price-dsc") {
        var listingsData = this.state.listingsData.sort(function (a, b) {
          return a.price - b.price;
        });

        this.setState({
          listingsData: listingsData
        });
      }

      this.setState({
        filterData: newData,
        searchResult: newData.length
      });

      console.log(this.state.searchResult);
    }
  }, {
    key: 'populateForms',
    value: function populateForms() {
      var _this4 = this;

      //neibourhood
      var neighbourhoods = this.state.listingsData.map(function (item) {
        return item.neighbourhood;
      });

      neighbourhoods = new Set(neighbourhoods);
      neighbourhoods = [].concat(_toConsumableArray(neighbourhoods));
      neighbourhoods = neighbourhoods.sort();

      //HouseTypes
      var houseTypes = this.state.listingsData.map(function (item) {
        return item.houseType;
      });

      houseTypes = new Set(houseTypes);
      houseTypes = [].concat(_toConsumableArray(houseTypes));
      houseTypes = houseTypes.sort();
      //city
      //var cities = this.state.listingsData.map((item)=> { return item.city })

      //cities = new set (cities)
      //cities = [...cities]

      //bedrooms

      //var bedrooms = this.state.listingsData.map((item)=> { return item.rooms })

      //bedrooms = new set (bedrooms)class
      //bedrooms = [...bedrooms]

      this.setState({
        populateFormsData: {
          neighbourhoods: neighbourhoods,
          houseTypes: houseTypes
          // cities
        }
      }, function () {
        console.log(_this4.state);
      });
    }
  }, {
    key: 'componentWillMount',
    value: function componentWillMount() {
      var listingsData = this.state.listingsData.sort(function (a, b) {
        return a.price - b.price;
      });

      this.setState({
        listingsData: listingsData
      });
    }
  }, {
    key: 'render',
    value: function render() {
      //console.log(this.state.listingsData)
      return _react2.default.createElement(
        'div',
        null,
        ' ',
        _react2.default.createElement(_Header2.default, null),
        _react2.default.createElement(
          'section',
          { id: 'content-area' },
          _react2.default.createElement(_Filter2.default, {
            change: this.change,
            globalState: this.state,
            populateAction: this.populateForms
          }),
          _react2.default.createElement(_Listings2.default, {
            listingsData: this.state.filterData,
            globalState: this.state,
            change: this.change
          })
        )
      );
    }
  }]);

  return App;
}(_react.Component);

var app = document.getElementById('app');

_reactDom2.default.render(_react2.default.createElement(App, null), app);

/***/ }),

/***/ 54:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var listingsData = [{
    address: 'Belgrano 42',
    city: 'Bahía Blanca',
    state: 'BB',
    neighbourhood: 'Centro',
    rooms: 3,
    price: 200,
    floorSpace: 234000,
    extras: ['elevator', 'gym'],
    houseType: 'Departamento Centrico',
    image: 'https://images.adsttc.com/media/images/5954/c1fe/b22e/38be/e300/0101/slideshow/2015_DEPTO_JSMH_SMA_PHOTO_by_Paul_Rivera__04.jpg?1498726906'
}, {
    address: 'Av. Alem 347',
    city: 'Cerri',
    state: 'BB',
    neighbourhood: 'Centro',
    rooms: 4,
    price: 2200,
    floorSpace: 2000,
    extras: ['elevator', 'gym'],
    houseType: 'Departamento',
    image: 'https://images.adsttc.com/media/images/5954/c259/b22e/38a4/6e00/0129/slideshow/2015_DEPTO_JSMH_SMA_PHOTO_by_Paul_Rivera__12.jpg?1498726997'
}, {
    address: 'Estomba 1300',
    city: 'Ing White',
    state: 'BB',
    neighbourhood: 'Palos Verdes',
    rooms: 1,
    price: 4200,
    floorSpace: 1890,
    extras: ['elevator', 'gym'],
    houseType: 'Departamento',
    image: 'https://images.adsttc.com/media/images/5954/c2ab/b22e/38be/e300/0109/slideshow/2015_DEPTO_JSMH_SMA_PHOTO_by_Paul_Rivera__21.jpg?1498727079'
}, {
    address: 'Witcomb 700',
    city: 'Medanos',
    state: 'BB',
    neighbourhood: 'Patagonia',
    rooms: 6,
    price: 1220,
    floorSpace: 2350,
    extras: ['elevator', 'gym'],
    houseType: 'Casa',
    image: 'https://images.adsttc.com/media/images/5954/c227/b22e/38be/e300/0103/slideshow/2015_DEPTO_JSMH_SMA_PHOTO_by_Paul_Rivera__07.jpg?1498726946'
}, {
    address: 'Chiclana 470',
    city: 'Bahía Blanca',
    state: 'BB',
    neighbourhood: 'Palihue',
    rooms: 3,
    price: 2300,
    floorSpace: 6076,
    extras: ['elevator', 'gym'],
    houseType: 'Casa',
    image: 'https://images.adsttc.com/media/images/5954/c24a/b22e/38a4/6e00/0128/slideshow/2015_DEPTO_JSMH_SMA_PHOTO_by_Paul_Rivera__10.jpg?1498726982'
}];

exports.default = listingsData;

/***/ }),

/***/ 98:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(16);

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Filter = function (_Component) {
  _inherits(Filter, _Component);

  function Filter() {
    _classCallCheck(this, Filter);

    var _this = _possibleConstructorReturn(this, (Filter.__proto__ || Object.getPrototypeOf(Filter)).call(this));

    _this.state = {
      name: 'RMC'
    };
    _this.neighbourhoods = _this.neighbourhoods.bind(_this);
    _this.houseTypes = _this.houseTypes.bind(_this);
    return _this;
  }

  _createClass(Filter, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      this.props.populateAction();
    }
  }, {
    key: 'neighbourhoods',
    value: function neighbourhoods() {
      if (this.props.globalState.populateFormsData.neighbourhoods != undefined) {
        var neighbourhoods = this.props.globalState.populateFormsData.neighbourhoods;

        return neighbourhoods.map(function (item) {
          return _react2.default.createElement(
            'option',
            { key: item, value: item },
            item
          );
        });
      }
    }
  }, {
    key: 'houseTypes',
    value: function houseTypes() {
      if (this.props.globalState.populateFormsData.houseTypes != undefined) {
        var houseTypes = this.props.globalState.populateFormsData.houseTypes;

        return houseTypes.map(function (item) {
          return _react2.default.createElement(
            'option',
            { key: item, value: item },
            item
          );
        });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'section',
        { id: 'filter' },
        _react2.default.createElement(
          'div',
          { className: 'inside' },
          _react2.default.createElement(
            'h4',
            null,
            'FILTER'
          ),
          _react2.default.createElement(
            'label',
            { htmlFor: 'neighbourhood' },
            'Neighbourhood'
          ),
          _react2.default.createElement(
            'select',
            { name: 'neighbourhood', className: 'filters neighbourhood', onChange: this.props.change },
            _react2.default.createElement(
              'option',
              { value: 'all' },
              'All'
            ),
            this.neighbourhoods()
          ),
          _react2.default.createElement(
            'label',
            { htmlFor: 'houseType' },
            'houseType'
          ),
          _react2.default.createElement(
            'select',
            { name: 'houseType', className: 'filters houseType', onChange: this.props.change },
            _react2.default.createElement(
              'option',
              { value: 'all' },
              'All'
            ),
            this.houseTypes()
          ),
          _react2.default.createElement(
            'label',
            { htmlFor: 'Interior' },
            'Interior'
          ),
          _react2.default.createElement(
            'select',
            { name: 'interior', className: 'filters interior', onChange: this.props.change },
            _react2.default.createElement(
              'option',
              { value: 'All' },
              'All'
            ),
            _react2.default.createElement(
              'option',
              { value: 'Amplio' },
              'Amplio'
            ),
            _react2.default.createElement(
              'option',
              { value: 'Mono Ambiente' },
              'Mono Ambiente'
            )
          ),
          _react2.default.createElement(
            'h5',
            null,
            'Price'
          ),
          ' ',
          _react2.default.createElement(
            'span',
            null,
            this.props.globalState.floor_price
          ),
          _react2.default.createElement(
            'div',
            { className: 'slidecontainer' },
            _react2.default.createElement('input', { type: 'range', name: 'floor_price', min: '0', max: '10000', value: this.props.globalState.floor_price, className: 'slider', id: 'floor_price', onChange: this.props.change })
          ),
          _react2.default.createElement(
            'h5',
            null,
            'Floor Space'
          ),
          ' ',
          _react2.default.createElement(
            'span',
            null,
            this.props.globalState.floor_space
          ),
          _react2.default.createElement(
            'div',
            { className: 'slidecontainer' },
            _react2.default.createElement('input', { type: 'range', name: 'floor_space', min: '0', max: '10000', value: this.props.globalState.floor_space, className: 'slider', id: 'floor_space', onChange: this.props.change })
          ),
          _react2.default.createElement(
            'h5',
            null,
            'Radius'
          ),
          ' ',
          _react2.default.createElement(
            'span',
            null,
            this.props.globalState.floor_radius
          ),
          _react2.default.createElement(
            'div',
            { className: 'slidecontainer' },
            _react2.default.createElement('input', { type: 'range', name: 'floor_radius', min: '0', max: '10000', value: this.props.globalState.floor_radius, className: 'slider', id: 'radius', onChange: this.props.change })
          ),
          _react2.default.createElement(
            'h5',
            null,
            'Facilities'
          ),
          _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              'label',
              { htmlFor: 'extras', className: 'checkbox' },
              ' ',
              _react2.default.createElement(
                'span',
                null,
                'Elevator'
              ),
              _react2.default.createElement('input', { type: 'checkbox', className: 'facilities', name: 'elevator', value: 'elevator', onChange: this.props.change })
            ),
            _react2.default.createElement(
              'label',
              { htmlFor: 'extras', className: 'checkbox' },
              _react2.default.createElement(
                'span',
                null,
                'Storage'
              ),
              _react2.default.createElement('input', { type: 'checkbox', className: 'facilities', name: 'storage', value: 'storage', onChange: this.props.change })
            ),
            _react2.default.createElement(
              'label',
              { htmlFor: 'extras', className: 'checkbox' },
              _react2.default.createElement(
                'span',
                null,
                'Bath tub'
              ),
              _react2.default.createElement('input', { type: 'checkbox', className: 'facilities', name: 'bath-tub', value: 'bath-tub', onChange: this.props.change })
            ),
            _react2.default.createElement(
              'label',
              { htmlFor: 'extras', className: 'checkbox' },
              _react2.default.createElement(
                'span',
                null,
                'Separator shower'
              ),
              _react2.default.createElement('input', { type: 'checkbox', className: 'facilities', name: 'separator-shower', value: 'separator-shower', onChange: this.props.change })
            ),
            _react2.default.createElement(
              'label',
              { htmlFor: 'extras', className: 'checkbox' },
              _react2.default.createElement(
                'span',
                null,
                'Fireplace'
              ),
              _react2.default.createElement('input', { type: 'checkbox', className: 'facilities', name: 'Fireplace', value: 'Fireplace', onChange: this.props.change })
            ),
            _react2.default.createElement(
              'label',
              { htmlFor: 'extras', className: 'checkbox' },
              _react2.default.createElement(
                'span',
                null,
                'Swiming pool'
              ),
              _react2.default.createElement('input', { type: 'checkbox', className: 'facilities', name: 'Swiming_pool', value: 'Swiming_pool', onChange: this.props.change })
            )
          )
        )
      );
    }
  }]);

  return Filter;
}(_react.Component);

exports.default = Filter;

/***/ }),

/***/ 99:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(16);

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Header = function (_Component) {
  _inherits(Header, _Component);

  function Header() {
    _classCallCheck(this, Header);

    var _this = _possibleConstructorReturn(this, (Header.__proto__ || Object.getPrototypeOf(Header)).call(this));

    _this.state = {
      name: 'RMC'
    };
    return _this;
  }

  _createClass(Header, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'header',
        null,
        _react2.default.createElement(
          'div',
          { className: 'logo' },
          'Logo'
        ),
        _react2.default.createElement(
          'nav',
          null,
          _react2.default.createElement(
            'a',
            { href: '#' },
            'Create Ads'
          ),
          _react2.default.createElement(
            'a',
            { href: '#' },
            'About Us'
          ),
          _react2.default.createElement(
            'a',
            { href: '#' },
            'Log In'
          ),
          _react2.default.createElement(
            'a',
            { href: '#', className: 'register-btn' },
            'Register'
          )
        )
      );
    }
  }]);

  return Header;
}(_react.Component);

exports.default = Header;

/***/ })

},[102]);