import React, { Component} from 'react'

export default class Filter extends Component {
  constructor () {
    super()
    this.state = {
      name: 'RMC'
    }
    this.neighbourhoods = this.neighbourhoods.bind(this)
    this.houseTypes = this.houseTypes.bind(this)
  }
  componentWillMount(){
    this.props.populateAction()
 
   }
  neighbourhoods(){
    if (this.props.globalState.populateFormsData.neighbourhoods != undefined){
      var {neighbourhoods} = this.props.globalState.populateFormsData;
      return neighbourhoods.map((item)=>{return(<option key={item} value={item}>{item}</option>)})
    }
  }
   

   houseTypes(){
    if (this.props.globalState.populateFormsData.houseTypes != undefined){
      var {houseTypes} = this.props.globalState.populateFormsData;
      return houseTypes.map((item)=>{return(<option key={item} value={item}>{item}</option>)})
    }
  }

  render () {
    return (
      
      <section id="filter">
       <div className="inside">
       <h4>FILTER</h4>
          
          <label htmlFor="neighbourhood">Neighbourhood</label>
          <select name="neighbourhood"  className="filters neighbourhood" onChange={this.props.change}>
            <option value="all">All</option>
            {this.neighbourhoods()}
          </select>
          
          <label htmlFor="houseType">houseType</label>
          <select name="houseType" className="filters houseType" onChange={this.props.change}>
            <option value="all">All</option>
            {this.houseTypes()}
          </select>
          
          <label htmlFor="Interior">Interior</label>
          <select name="interior" className="filters interior" onChange={this.props.change}>
            <option value="All">All</option>
            <option value="Amplio">Amplio</option>
            <option value="Mono Ambiente">Mono Ambiente</option>
          </select>

          <h5>Price</h5> <span>{this.props.globalState.floor_price}</span>
          <div className="slidecontainer">
            <input type="range"  name="floor_price" min="0" max="10000" value={this.props.globalState.floor_price} className="slider" id="floor_price" onChange={this.props.change} />
          </div>

          <h5>Floor Space</h5> <span>{this.props.globalState.floor_space}</span>
          <div className="slidecontainer">
            <input type="range" name="floor_space" min="0" max="10000" value={this.props.globalState.floor_space} className="slider" id="floor_space" onChange={this.props.change} />
          </div>
          
          <h5>Radius</h5> <span>{this.props.globalState.floor_radius}</span>
          <div className="slidecontainer">
            <input type="range" name="floor_radius" min="0" max="10000" value={this.props.globalState.floor_radius} className="slider" id="radius" onChange={this.props.change} />
          </div>
          
          <h5>Facilities</h5>
          <div>
            <label htmlFor="extras" className="checkbox"> <span>Elevator</span> 
              <input type="checkbox" className="facilities" name="elevator" value="elevator" onChange={this.props.change} />
            </label>
         
            <label htmlFor="extras" className="checkbox"><span>Storage</span> 
              <input type="checkbox" className="facilities" name="storage" value="storage" onChange={this.props.change} />
            </label>
         
            <label htmlFor="extras" className="checkbox"><span>Bath tub</span> 
              <input type="checkbox" className="facilities" name="bath-tub" value="bath-tub" onChange={this.props.change} />
            </label>
          
            <label htmlFor="extras" className="checkbox"><span>Separator shower</span> 
              <input type="checkbox" className="facilities" name="separator-shower" value="separator-shower" onChange={this.props.change} />
            </label>
          
            <label htmlFor="extras" className="checkbox"><span>Fireplace</span> 
            <input type="checkbox" className="facilities" name="Fireplace" value="Fireplace" onChange={this.props.change} />
          </label>
         
            <label htmlFor="extras" className="checkbox"><span>Swiming pool</span> 
              <input type="checkbox" className="facilities" name="Swiming_pool" value="Swiming_pool" onChange={this.props.change} />
            </label>
          </div>
          </div>
      </section>
    )
  }
}


