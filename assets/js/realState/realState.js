import React, { Component} from 'react'
import ReactDOM from 'react-dom'
import Header from './Header'
import Filter from './Filter'
import Listings from './Listings'
import listingsData from './data/listingsData.js'

class App extends Component {
  constructor () {
    super()
    this.state = {
      name: 'RMC',
      listingsData,
      neighbourhood:"All",
      houseType: "All",
      interior:"All",
      floor_price: 0,
      floor_space: 0,
      floor_radius: 0,
      filterData: listingsData,
      searchResult: listingsData.length,
      populateFormsData: {},
      sortby:"price-asc"
    }

    this.change = this.change.bind(this)
    this.filterData = this.filterData.bind(this)
    this.populateForms = this.populateForms.bind(this)

  }
  change(event){
    var name  = event.target.name
    var value = (event.target.type === 'checkbox') ? event.target.checked : event.target.value
    
    this.setState({
      [name]: value
    },()=>{
      console.log(this.state)
      this.filterData()
    })
  }

  filterData(){
    var newData = this.state.listingsData.filter((item)=>{
      return item.price >= this.state.floor_price && item.floorSpace >= this.state.floor_space
    })
    
    if (this.state.neighbourhood != "All"){
      newData = newData.filter((item)=>{
        return item.neighbourhood == this.state.neighbourhood

      })
    }

    if (this.state.houseType != "All"){
      newData = newData.filter((item)=>{
        return item.houseType == this.state.houseType

      })
    }
    
    if(this.state.sortby == "price-asc"){
      var listingsData = this.state.listingsData.sort((a,b)=>{
        return b.price - a.price
      })
  
      this.setState({
        listingsData
      })
    }

    if(this.state.sortby == "price-dsc"){
      var listingsData = this.state.listingsData.sort((a,b)=>{
        return a.price - b.price
      })
  
      this.setState({
        listingsData
      })
    }

    

    this.setState({
      filterData: newData,
      searchResult: newData.length
    })


    console.log(this.state.searchResult)
  }

  populateForms(){
    //neibourhood
    var neighbourhoods = this.state.listingsData.map((item)=> { return item.neighbourhood })
    
    neighbourhoods = new Set (neighbourhoods)
    neighbourhoods = [...neighbourhoods]
    neighbourhoods = neighbourhoods.sort()
 
    //HouseTypes
    var houseTypes = this.state.listingsData.map((item)=> { return item.houseType })
    
    houseTypes = new Set (houseTypes)
    houseTypes = [...houseTypes]
    houseTypes = houseTypes.sort()
    //city
    //var cities = this.state.listingsData.map((item)=> { return item.city })
    
    //cities = new set (cities)
    //cities = [...cities]
 
    //bedrooms

    //var bedrooms = this.state.listingsData.map((item)=> { return item.rooms })
    
    //bedrooms = new set (bedrooms)class
    //bedrooms = [...bedrooms]

    this.setState({
      populateFormsData:{
        neighbourhoods,
        houseTypes,
       // cities
      }    
    }, ()=>{console.log(this.state)}
    )

  }

  componentWillMount()
  {
    var listingsData = this.state.listingsData.sort((a,b)=>{
      return a.price - b.price
    })

    this.setState({
      listingsData
    })
  }

  render () { 
    //console.log(this.state.listingsData)
    return (<div> <Header />
      <section id="content-area">
        <Filter 
          change={this.change} 
          globalState={this.state}
         populateAction={this.populateForms}
        />
        <Listings 
          listingsData={this.state.filterData} 
          globalState={this.state}
          change={this.change}
        />
      </section>
    </div>)
  }
}

const app = document.getElementById('app')

ReactDOM.render(<App />, app)
