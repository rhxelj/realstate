var listingsData = [
    {
        address: 'Belgrano 42',
        city: 'Bahía Blanca',
        state: 'BB',
        neighbourhood: 'Centro',
        rooms: 3,
        price: 200,
        floorSpace: 234000,
        extras: [
            'elevator',
            'gym'
        ],
        houseType: 'Departamento Centrico',
        image:'https://images.adsttc.com/media/images/5954/c1fe/b22e/38be/e300/0101/slideshow/2015_DEPTO_JSMH_SMA_PHOTO_by_Paul_Rivera__04.jpg?1498726906'
    },
    {
        address: 'Av. Alem 347',
        city: 'Cerri',
        state: 'BB',
        neighbourhood: 'Centro',
        rooms: 4,
        price: 2200,
        floorSpace: 2000,
        extras: [
            'elevator',
            'gym'
        ],
        houseType: 'Departamento',
        image:'https://images.adsttc.com/media/images/5954/c259/b22e/38a4/6e00/0129/slideshow/2015_DEPTO_JSMH_SMA_PHOTO_by_Paul_Rivera__12.jpg?1498726997'
    },
    {
        address: 'Estomba 1300',
        city: 'Ing White',
        state: 'BB',
        neighbourhood: 'Palos Verdes',
        rooms: 1,
        price: 4200,
        floorSpace: 1890,
        extras: [
            'elevator',
            'gym'
        ],
        houseType: 'Departamento',
        image:'https://images.adsttc.com/media/images/5954/c2ab/b22e/38be/e300/0109/slideshow/2015_DEPTO_JSMH_SMA_PHOTO_by_Paul_Rivera__21.jpg?1498727079'
    },
    {
        address: 'Witcomb 700',
        city: 'Medanos',
        state: 'BB',
        neighbourhood: 'Patagonia',
        rooms: 6,
        price: 1220,
        floorSpace: 2350,
        extras: [
            'elevator',
            'gym'
        ],
        houseType: 'Casa',
        image:'https://images.adsttc.com/media/images/5954/c227/b22e/38be/e300/0103/slideshow/2015_DEPTO_JSMH_SMA_PHOTO_by_Paul_Rivera__07.jpg?1498726946'
    },
    {
        address: 'Chiclana 470',
        city: 'Bahía Blanca',
        state: 'BB',
        neighbourhood: 'Palihue',
        rooms: 3,
        price: 2300,
        floorSpace: 6076,
        extras: [
            'elevator',
            'gym'
        ],
        houseType: 'Casa',
        image:'https://images.adsttc.com/media/images/5954/c24a/b22e/38a4/6e00/0128/slideshow/2015_DEPTO_JSMH_SMA_PHOTO_by_Paul_Rivera__10.jpg?1498726982'
    },
]

export default listingsData;