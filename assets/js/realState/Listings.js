import React, { Component } from 'react'
import listingsData from './data/listingsData';

export default class Listings extends Component {
  constructor () {
    super()
    this.state = {
      name: 'RMC'
    }
    this.loopListings = this.loopListings.bind(this)
  }
  
  loopListings (){
    
    var {listingsData} = this.props

    if(listingsData == undefined || listingsData.length == 0){
      return "Sorry your filter did not match any listing"
    }
    
    return listingsData.map((listing,index)=>{
      return (
        <div className="card col-md-3" key={index}>
        <div className="listing">
          <div className="listing-img" style={{background: `url('${listing.image}') no-repeat center center`}}>
            <span className="adress">{listing.address}</span>
            <div className="details">
              <div className="col-md-3">
                <div className="user-img"></div>
              </div>
              <div className="col-md-9"> 
                <div className="user-details">
                  <span className="user-name">Nina Smith</span>
                  <span className="post-date">05/05/2017</span>
                </div>
                <div className="listing-details">
                    <div className="floor-space"><i className="far fa-square"></i> <span>{listing.floorSpace} m&#178;</span></div>
                    <div className="bedrooms"><i className="fas fa-bed"></i><span>{listing.rooms} Bedrooms</span> </div>
                    <div className=""></div>
                    <div className=""></div>
                </div>
                <div className="view-btn">View Listing</div>
              </div>  
            </div>
          </div>
          <div className="bottom-info">
              <span className="price">$ {listing.price} / month</span>
              
              <span className="location"><i className="fas fa-map-marker"></i>{listing.city}</span>
            </div>
        </div>
      </div>
      )
    } )

  }
  render () {
    return (
      <section id="listings">
        <section className="search-area">
          <input type="text" name="search"/>
        </section>

        <section className="sortby-area">
          <div className="results"> {this.props.globalState.searchResult} results found</div>
          <div className="sort-options">
            <select name="sortby" className="sortby" onChange={this.props.change}>
              <option value="price-asc">Lowest Price</option>
              <option value="price-dsc">Highest Price</option>
            </select>
            <div className="view">
              <i className="fas fa-list"></i>
              <i className="fas fa-th"></i>
            </div>
          </div>
        </section>

        
          <section className="listing-results">
              {this.loopListings()}
          </section>
       
        <section id="pagination">
          <ul className="pages">
            <li>Prev</li>
            <li className="active">1</li>
            <li>2</li>
            <li>3</li>
            <li>4</li>
            <li>5</li>
            <li>6</li>
            <li>Next</li>
          </ul>
        </section>

      </section>
    )
  }
}


